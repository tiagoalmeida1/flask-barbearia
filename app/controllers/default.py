from app import app
from flask import request, jsonify
import psycopg2 
import json

#listar todos os clientes
@app.route("/", methods=['GET'])
def index():
    con = psycopg2.connect("""dbname=barbearia
                        user=postgres password=root
                        host=localhost port=5432""")
    cur = con.cursor()
    cur.execute("SELECT * FROM cliente")
    res = jsonify(cur.fetchall())
    return res

#cadastrar um usuario novo
@app.route('/cadastrar', methods=['POST'])
def inserir():
    nome = request.json['nome']
    senha = request.json['senha']
    telefone = request.json['telefone']
    email = request.json['email']

    con = psycopg2.connect("""dbname=barbearia
                        user=postgres password=root
                        host=localhost port=5432""")
    cur = con.cursor()

    if (email != ''):
        cur.execute("INSERT INTO cliente (nome, senha, telefone, email) VALUES (%s, %s, %s, %s)", (nome, senha, telefone, email))
    elif (email == ''):
        cur.execute("INSERT INTO cliente (nome, senha, telefone) VALUES (%s, %s, %s)", (nome, senha, telefone))
    #sql = "INSERT INTO alunos VALUES({0}, {1}, '{2}', {3}, {4}, '{5}',);".format(mat, cod, data, cred, mgp, nome)
    #cur.execute("INSERT INTO alunos (mat_alu, cod_curso, dat_nasc, tot_cred, mgp, nom_alu) VALUES (%s, %s, %s, %s, %s, %s)", (mat, cod, data, cred, mgp, nome,))
    con.commit()
    
    cur.execute("SELECT * FROM cliente")

    return jsonify(cur.fetchall())

#listar todos na fila
@app.route('/fila', methods=['GET'])
def fila():
    con = psycopg2.connect("""dbname=barbearia
                        user=postgres password=root
                        host=localhost port=5432""")
    cur = con.cursor()
    cur.execute("SELECT * FROM fila ORDER BY id ")
    return jsonify(cur.fetchall())


#usuario entrar na fila
@app.route('/entrar', methods=['POST'])
def entrar():
    nome = request.json['nome']
    telefone = request.json['telefone']

    con = psycopg2.connect("""dbname=barbearia
                        user=postgres password=root
                        host=localhost port=5432""")
    cur = con.cursor()
    cur.execute("INSERT INTO fila (nome, telefone) VALUES (%s, %s)", (nome, telefone))
    con.commit()

    cur.execute("SELECT * FROM fila")
    return jsonify(cur.fetchall())